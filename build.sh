#!/bin/bash
git pull

# 删除原容器
docker rm -f recruit-vue

# 删除原镜像
docker rmi -f recruit-vue

yarn run build

# 构建镜像
docker build -t recruit-vue -f Dockerfile .

# 启动容器
docker run -d -p 8095:8095 --name recruit-vue recruit-vue

#11