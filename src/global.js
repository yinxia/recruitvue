
export default {
  URLprotocol: window.location.protocol,
  URLhost: window.location.host,
  URL: window.location.protocol +
    '//' + window.location.host,
  baseURL: process.env.VUE_APP_URL ? process.env.VUE_APP_URL : 'http://192.168.0.185:8095/',

  /**
   *设置cookie
   * @param  c_name key名   
   * @param  value key值
   * @param  expire 有效时间
   * @returns 
   */
  setCookie(c_name, value, expire) {
    var date = new Date()
    date.setSeconds(date.getSeconds() + expire)
    document.cookie = c_name + "=" + encodeURI(value, "utf-8") + "; expires=" + date.toGMTString()
  },

  /**
   *设置cookie
   * @param  c_name key名
   * @returns ‘’
   */
  getCookie(c_name) {
    if (document.cookie.length > 0) {
      let c_start = document.cookie.indexOf(c_name + "=")
      if (c_start != -1) {
        c_start = c_start + c_name.length + 1
        let c_end = document.cookie.indexOf(";", c_start)
        if (c_end == -1) c_end = document.cookie.length
        return decodeURI(document.cookie.substring(c_start, c_end), "utf-8")
      }
    }
    return ""
  },

  /**
   *删除cookie
   * @param  c_name key名
   * @returns
   */
  delCookie(c_name) {
    this.setCookie(c_name, "", -1)
  },

  // 动态获取路由
  getMeau() {
    // let data;
    // axios.get('addresslist/api/sysMenu/treeSysMenu').then(res=>{
    //    data=this.MeauList;
    // })
    return this.MeauList;
    // return [
    //   // =======================      功能模块 start      =======================
    //   {
    //     id: '1',// 你的id存储
    //     path: '/functionModule',//路径名称
    //     name: 'functionModule',// 组件名称
    //     component: 'Layout',//组件路径
    //     meta: {
    //       title: '功能选择',//菜单名称
    //       keepAlive: true,
    //       keepShow: true,//可见
    //       role: ['admin'], //权限设置
    //       show: ['ROLE_ADMIN', 'ROLE_MANAGE', 'ROLE_EMPLOYEE'], //权限设置,
    //       icon: 'el-icon-setting' //菜单左侧的icon图标
    //     },
    //     children: [
    //       {
    //         id: '12',
    //         path: '/functionModule/permissionSetting',
    //         name: 'permissionSetting',
    //         meta: {
    //           title: '系统设置',
    //           keepShow: true,
    //           role: ['admin', 'manage', 'infoCenter'], //权限设置
    //           show: ['ROLE_ADMIN', 'ROLE_MANAGE', 'ROLE_EMPLOYEE'], //权限设置 ,
    //           icon: 'el-icon-setting' //菜单左侧的icon图标
    //         },
    //         component: '/functionModule/index',
    //         redirect: '/functionModule/permissionSetting/menu',
    //         children: [
    //           // {
    //           //   id: '121',
    //           //   name: 'departManage',
    //           //   path: 'departManage',
    //           //   meta: {
    //           //     title: '部门管理',
    //           //     keepShow: true,
    //           //     role: ['admin', 'manage', 'employee'], //权限设置
    //           //     icon: 'el-icon-connection' //菜单左侧的icon图标
    //           //   },
    //           //   component: '/functionModule/departManage',
    //           // },
    //           // {
    //           //   id: '122',
    //           //   name: 'postManage',
    //           //   path: 'postManage',
    //           //   meta: {
    //           //     title: '岗位管理',
    //           //     keepShow: true,
    //           //     role: ['admin', 'manage', 'employee',], //权限设置
    //           //     icon: 'el-icon-connection' //菜单左侧的icon图标
    //           //   },
    //           //   component: '/functionModule/postManage',
    //           // },
    //           {
    //             id: '122',
    //             name: 'roleManage',
    //             path: 'roleManage',
    //             meta: {
    //               title: '角色权限',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee',], //权限设置
    //               icon: 'el-icon-s-custom' //菜单左侧的icon图标
    //             },
    //             component: '/functionModule/roleManage',
    //           },
    //           {
    //             id: '123',
    //             path: 'userSettings',
    //             name: 'UserSettings',
    //             meta: {
    //               title: '用户设置',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'infoCenter'], //权限设置
    //               show: ['ROLE_ADMIN', 'ROLE_MANAGE', 'ROLE_EMPLOYEE'], //权限设置 ,
    //               icon: 'el-icon-setting' //菜单左侧的icon图标
    //             },
    //             component: '/usercenter/UserSettings'
    //           },
    //           {
    //             id: '121',
    //             path: 'menu',
    //             name: 'menu',
    //             meta: {
    //               title: '菜单管理',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'infoCenter'], //权限设置
    //               show: ['ROLE_ADMIN', 'ROLE_MANAGE', 'ROLE_EMPLOYEE'], //权限设置 ,
    //               icon: 'el-icon-setting' //菜单左侧的icon图标
    //             },
    //             component: '/functionModule/MenuManage'
    //           },
    //         ]
    //       },
    //       {
    //         id: '13',
    //         path: '/functionModule/organizeStructure',
    //         name: 'organizeStructure',
    //         meta: {
    //           title: '组织结构',
    //           keepShow: true,
    //           role: ['admin', 'manage', 'infoCenter'], //权限设置
    //           show: ['ROLE_ADMIN', 'ROLE_MANAGE', 'ROLE_EMPLOYEE'], //权限设置 ,
    //           icon: 'el-icon-setting' //菜单左侧的icon图标
    //         },
    //         component: '/functionModule/organizeStructure'
    //       },

    //     ],
    //     redirect: '/functionModule/permissionSetting',
    //   },
    //   // =======================      功能模块 end      =======================

    //   // =======================      信息管理中心 start      =======================
    //   {
    //     id: '2',
    //     path: '/nmimc',
    //     name: 'nmimc',
    //     component: 'Layout',
    //     meta: {
    //       title: '信息管理中心',
    //       keepAlive: true,
    //       role: ['admin', 'infoCenter'], //权限设置
    //       // show: ['ROLE_ADMIN', 'ROLE_MANAGE', 'ROLE_EMPLOYEE'], //权限设置,
    //       icon: 'el-icon-s-unfold' //菜单左侧的icon图标
    //     },
    //     children: [{
    //       id: '21',
    //       path: 'manager',
    //       name: 'manager',
    //       meta: {
    //         title: '首页',
    //         role: ['admin', 'manage', 'infoCenter'], //权限设置
    //         keepShow: true,
    //         show: ['ROLE_ADMIN', 'ROLE_MANAGE', 'ROLE_EMPLOYEE'], //权限设置 ,
    //         icon: 'el-icon-s-home' //菜单左侧的icon图标
    //       },
    //       component: '/business/networkChain/NetworkChain'
    //     }]
    //   },
    //   // =======================      信息管理中心 end      =======================

    //   // =======================      项目部 start      =======================
    //   {
    //     id: '3',
    //     path: '/project',
    //     name: 'project',
    //     component: 'Layout',
    //     meta: {
    //       title: '项目部',
    //       keepAlive: true,
    //       role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //       show: ['ROLE_ADMIN', 'ROLE_MANAGE', 'ROLE_EMPLOYEE'], //权限设置,
    //       icon: 'el-icon-info' //菜单左侧的icon图标
    //     },

    //     children: [
    //       // ========== 站点匹配进度管理 start ========== 
    //       {
    //         id: '31',
    //         name: 'siteMatch',
    //         path: '/project/siteMatch',
    //         meta: {
    //           title: '站点匹配进度管理',
    //           keepShow: true,
    //           role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //           icon: 'el-icon-star-on' //菜单左侧的icon图标
    //         },
    //         component: '/project/siteMatch/index',
    //         redirect: '/project/siteMatch/proIntegratorMatchProgress',
    //         children: [
    //           {
    //             id: '311',
    //             name: 'proIntegratorMatchProgress',
    //             path: 'proIntegratorMatchProgress',
    //             meta: {
    //               title: '项目集成商匹配进度',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-connection' //菜单左侧的icon图标
    //             },
    //             component: '/project/siteMatch/proIntegratorMatchProgress',
    //           },
    //           {
    //             id: '312',
    //             name: 'proIntegratorDetail',
    //             path: 'proIntegratorDetail',
    //             meta: {
    //               title: '项目集成商详情',
    //               keepShow: false,
    //               role: [], //权限设置
    //               icon: 'el-icon-pear' //菜单左侧的icon图标
    //             },
    //             component: '/project/siteMatch/proIntegratorDetail',
    //           },
    //           {
    //             id: '313',
    //             name: 'reportIntegrator',
    //             path: 'reportIntegrator',
    //             meta: {
    //               title: '上报集成商',
    //               keepShow: false,
    //               role: [], //权限设置
    //               icon: 'el-icon-goods' //菜单左侧的icon图标
    //             },
    //             component: '/project/siteMatch/reportIntegrator',
    //           },
    //           {
    //             id: '314',
    //             name: 'reported',
    //             path: 'reported',
    //             meta: {
    //               title: '已上报',
    //               keepShow: false,
    //               role: [], //权限设置
    //               icon: 'el-icon-pear' //菜单左侧的icon图标
    //             },
    //             component: '/project/siteMatch/reported',
    //           },
    //           // {
    //           //   id: '315',
    //           //   name: 'proMatchgather',
    //           //   path: 'proMatchgather',
    //           //   meta: {
    //           //     title: '项目匹配汇总',
    //           //     keepShow: false,
    //           //     role: ['admin',], //权限设置
    //           //     icon: 'el-icon-folder' //菜单左侧的icon图标
    //           //   },
    //           //   component: '/project/siteMatch/proMatchgather',
    //           // },
    //           // {
    //           //   id: '316',
    //           //   name: 'proLargesummary',
    //           //   path: 'proLargesummary',
    //           //   meta: {
    //           //     title: '项目大汇总',
    //           //     keepShow: false,
    //           //     role: ['admin'], //权限设置
    //           //     icon: 'el-icon-folder-opened' //菜单左侧的icon图标
    //           //   },
    //           //   component: '/project/siteMatch/proLargesummary',
    //           // },
    //           {
    //             id: '317',
    //             name: 'integratoradministration',
    //             path: 'integratoradministration',
    //             meta: {
    //               title: '集成商上报',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-s-cooperation' //菜单左侧的icon图标
    //             },
    //             component: '/project/siteMatch/integratoradministration',

    //           },
    //           {
    //             id: '318',
    //             name: 'reportedsite',
    //             path: 'reportedsite',
    //             meta: {
    //               title: '上报站点',
    //               keepShow: false,
    //               role: [], //权限设置
    //               icon: 'el-icon-s-data' //菜单左侧的icon图标
    //             },
    //             component: '/project/siteMatch/reportedsite',
    //           },


    //         ]
    //       },
    //       // ========== 站点匹配进度管理 end ========== 

    //       // ========== 项目信息管理 start ========== 
    //       {
    //         id: '32',
    //         name: 'projectInfoManage',
    //         path: '/project/projectInfoManage',
    //         meta: {
    //           title: '项目信息管理',
    //           keepShow: true,
    //           role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //           icon: 'el-icon-s-unfold' //菜单左侧的icon图标
    //         },
    //         component: '/project/projectInfoManage/index',
    //         children: [
    //           {
    //             id: '321',
    //             name: 'projectInfoSummary',
    //             path: 'projectInfoSummary',
    //             meta: {
    //               title: '项目信息汇总',
    //               keepShow: false,
    //               role: ['admin',], //权限设置
    //               icon: 'el-icon-s-order' //菜单左侧的icon图标
    //             },
    //             component: '/project/projectInfoManage/projectInfoSummary',
    //           },
    //           {
    //             id: '322',
    //             name: 'projectInfoManage',
    //             path: 'projectInfoManage',
    //             meta: {
    //               title: '项目信息管理',
    //               keepShow: true,
    //               role: [], //权限设置
    //               icon: 'el-icon-s-order' //菜单左侧的icon图标
    //             },
    //             component: '/project/projectInfoManage/projectInfoManage',
    //           },
    //           // ========== 项目场景管理 start ========== 
    //           {
    //             id: '323',
    //             name: 'projectSceneList',
    //             path: 'projectSceneList',
    //             meta: {
    //               title: '项目场景',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-office-building' //菜单左侧的icon图标
    //             },
    //             component: '/project/projectInfoManage/projectSceneList',
    //           },
    //           {
    //             id: '324',
    //             name: 'projectScene',
    //             path: 'projectScene',
    //             meta: {
    //               title: '项目场景添加',
    //               keepShow: false,
    //               role: [], //权限设置
    //               icon: 'el-icon-office-building' //菜单左侧的icon图标
    //             },
    //             component: '/project/projectInfoManage/projectScene',
    //           },
    //           {
    //             id: '325',
    //             name: 'projectList',
    //             path: 'projectList',
    //             meta: {
    //               title: '项目清单',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-s-order' //菜单左侧的icon图标
    //             },
    //             component: '/project/projectInfoManage/projectList',
    //           },
    //           {
    //             id: '3251',
    //             name: 'projectListTemplate',
    //             path: 'projectListTemplate',
    //             meta: {
    //               title: '项目清单模板管理',
    //               keepShow: false,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-s-order' //菜单左侧的icon图标
    //             },
    //             component: '/project/projectInfoManage/projectListTemplate',
    //           },
    //           {
    //             id: '3261',
    //             name: 'proListAddandEditeV2',
    //             path: 'proListAddandEditeV2',
    //             meta: {
    //               title: '项目清单增删改查2',
    //               keepShow: false,
    //               role: [], //权限设置
    //               icon: 'el-icon-s-order' //菜单左侧的icon图标
    //             },
    //             // 'admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter','visitor'
    //             component: '/project/projectInfoManage/proListAddandEditeV2.0',
    //           },
    //           {
    //             id: '326',
    //             name: 'proListAddandEdite',
    //             path: 'proListAddandEdite',
    //             meta: {
    //               title: '项目清单增删改查',
    //               keepShow: false,
    //               role: [],//权限设置
    //               icon: 'el-icon-s-order' //菜单左侧的icon图标
    //             },
    //             component: '/project/projectInfoManage/proListAddandEdite',
    //           },
    //           // ========== 项目场景管理 end ========== 

    //         ]
    //       },
    //       // ========== 项目信息管理 end ========== 


    //       // ========== 集成商管理 start ========== 
    //       {
    //         name: 'integratorManage',
    //         path: '/project/integratorManage',
    //         meta: {
    //           title: '集成商管理',
    //           keepShow: true,
    //           role: ['admin', 'manage', 'employee', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //           icon: 'el-icon-s-unfold' //菜单左侧的icon图标
    //         },
    //         component: '/project/integratorManage/index',
    //         children: [
    //           {
    //             name: 'integratorSite',
    //             path: 'integratorSite',
    //             meta: {
    //               title: '集成商站点',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-s-order' //菜单左侧的icon图标
    //             },
    //             component: '/project/integratorManage/integratorSite',
    //           },
    //           {
    //             name: 'bidsSummary',
    //             path: 'bidsSummary',
    //             meta: {
    //               title: '招投标汇总',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-s-data' //菜单左侧的icon图标
    //             },
    //             component: '/project/integratorManage/bidsSummary',
    //           },
    //           {
    //             name: 'biddingManage',
    //             path: 'biddingManage',
    //             meta: {
    //               title: '招投标管理',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-s-order' //菜单左侧的icon图标
    //             },
    //             component: '/project/integratorManage/biddingManage',
    //           },
    //           {
    //             name: 'integratorData',
    //             path: 'integratorData',
    //             meta: {
    //               title: '集成商资料',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-s-order' //菜单左侧的icon图标
    //             },
    //             component: '/project/integratorManage/integratorData',
    //           },
    //           {
    //             name: 'integratorPolicy',
    //             path: 'integratorPolicy',
    //             meta: {
    //               title: '集成商政策管理',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-s-cooperation' //菜单左侧的icon图标
    //             },
    //             component: '/project/integratorManage/integratorPolicy',
    //           },
    //           {
    //             name: 'integratorPolicyTmpl',
    //             path: 'integratorPolicyTmpl',
    //             meta: {
    //               title: '集成商政策模板',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-s-cooperation' //菜单左侧的icon图标
    //             },
    //             component: '/project/integratorManage/integratorPolicyTmpl',
    //           },

    //         ]
    //       }
    //       // ========== 集成商管理 end ========== 
    //     ],
    //     redirect: '/project/integratorManage/integratorData',
    //   },
    //   // =======================      项目部 end      =======================

    //   // =======================      商务部 start      =======================
    //   {
    //     id: '4',
    //     path: '/business',
    //     name: 'business',
    //     component: 'Layout',
    //     meta: {
    //       title: '商务部',
    //       keepAlive: true,
    //       keepShow: true,
    //       role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //       show: ['ROLE_ADMIN', 'ROLE_MANAGE', 'ROLE_EMPLOYEE',], //权限设置
    //       icon: 'el-icon-info' //菜单左侧的icon图标
    //     },
    //     redirect: '/business/buildInformManage',
    //     children: [
    //       {
    //         id: '41',
    //         name: 'networkChain',
    //         path: '/business/networkChain',
    //         meta: {
    //           title: '人脉链',
    //           keepShow: true,
    //           role: ['admin', 'manage'], //权限设置
    //           icon: 'el-icon-s-custom' //菜单左侧的icon图标
    //         },
    //         component: '/business/networkChain/NetworkChain',
    //       },
    //       {
    //         id: '42',
    //         name: 'buildInformManage',
    //         path: '/business/buildInformManage',
    //         meta: {
    //           title: '楼宇信息管理',
    //           keepShow: true,
    //           role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //           icon: 'el-icon-s-unfold' //菜单左侧的icon图标
    //         },
    //         component: '/business/buildInformManage/index',
    //         redirect: '/business/buildInformManage/list',
    //         children: [
    //           // {

    //           //   name: 'sitesummary',
    //           //   path: 'sitesummary',
    //           //   meta: {
    //           //     title: '站点信息汇总',
    //           //     keepShow: true,
    //           // role: ['admin', 'manage', 'employee','ESalesman','BExecutor','BUM','PExecutor','PM','integrator','AE','infoCenter'], //权限设置
    //           //     icon: 'el-icon-document' //菜单左侧的icon图标
    //           //   },
    //           //   component: '/business/buildInformManage/Buildingsummary',
    //           // },
    //           // {
    //           //   id: '421',
    //           //   name: 'Buildingsummary',
    //           //   path: 'Buildingsummary',
    //           //   meta: {
    //           //     title: '楼宇汇总',
    //           //     keepShow: true,
    //           //     role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter'], //权限设置
    //           //     icon: 'el-icon-document' //菜单左侧的icon图标
    //           //   },
    //           //   component: '/business/buildInformManage/Buildingsummary',
    //           // },
    //           {
    //             id: '4288',
    //             name: 'BuildingSummary_2.0',
    //             path: 'BuildingSummary_2.0',
    //             meta: {
    //               title: '楼宇汇总',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-document' //菜单左侧的icon图标
    //             },
    //             component: '/business/buildInformManage/BuildingSummary_2.0',
    //           },
    //           {
    //             id: '4223',
    //             name: 'buildingMachMap2',
    //             path: 'buildingMachMap2',
    //             meta: {
    //               title: '楼宇匹配地图',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-document' //菜单左侧的icon图标
    //             },
    //             component: '/business/buildInformManage/buildingMachMap2',
    //           },
    //           {
    //             id: '423',
    //             name: 'list',
    //             path: 'list',
    //             meta: {
    //               title: '楼宇信息列表',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-document' //菜单左侧的icon图标
    //             },
    //             component: '/business/buildInformManage/BuildInformList',
    //           },
    //           {
    //             id: '424',
    //             path: "planfollow",
    //             name: 'planfollow',
    //             meta: {
    //               title: "计划跟进",
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-edit-outline' //菜单左侧的icon图标
    //             },
    //             component: '/business/buildInformManage/PlanFollow-up'
    //           },
    //           {
    //             id: '4211',
    //             path: "followupexamine",
    //             name: 'followupexamine',
    //             meta: {
    //               title: "计划审核",
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-edit-outline' //菜单左侧的icon图标
    //             },
    //             component: '/business/buildInformManage/followupexamine'
    //           },
    //           {
    //             id: '4212',
    //             path: "Projectsummary",
    //             name: 'Projectsummary',
    //             meta: {
    //               title: "项目汇总",
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-edit-outline' //菜单左侧的icon图标
    //             },
    //             component: '/business/buildInformManage/Projectsummary'
    //           },
    //           {
    //             id: '425',
    //             path: "examineVerify",
    //             name: 'examineVerify',
    //             meta: {
    //               title: "审核上报",
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-printer' //菜单左侧的icon图标
    //             },
    //             component: '/business/buildInformManage/examineVerify'
    //           },
    //           // {
    //           //   id: '426',
    //           //   path: "examineVerify_reprot",
    //           //   name: 'examineVerify_reprot',
    //           //   meta: {
    //           //     title: "审核上报",
    //           //     keepShow: true,
    //           // role: ['admin', 'manage', 'employee','ESalesman','BExecutor','BUM','PExecutor','PM','integrator','AE','infoCenter'], //权限设置
    //           //     icon: 'el-icon-upload' //菜单左侧的icon图标
    //           //   },
    //           //   component: '/business/buildInformManage/examineVerify_reprot'
    //           // },
    //           {
    //             id: '427',
    //             path: "projectFollow",
    //             name: 'projectFollow',
    //             meta: {
    //               title: "项目跟进",
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'],//权限设置
    //               icon: 'el-icon-news' //菜单左侧的icon图标
    //             },
    //             component: '/business/buildInformManage/projectFollow'
    //           },
    //           {
    //             id: '428',
    //             path: "renounceSite",
    //             name: 'renounceSite',
    //             meta: {
    //               title: "废弃站点",
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'], //权限设置
    //               icon: 'el-icon-delete' //菜单左侧的icon图标
    //             },
    //             component: '/business/buildInformManage/renounceSite'
    //           },
    //           {
    //             id: '429',
    //             path: "Buildingdetails",
    //             name: 'Buildingdetails',
    //             meta: {
    //               title: "详情",
    //               keepShow: false,
    //               role: [], //权限设置
    //               icon: 'el-icon-s-management' //菜单左侧的icon图标
    //             },
    //             component: '/business/buildInformManage/Buildingdetailsinfo'
    //           },
    //           {
    //             id: '4210',
    //             path: "newtailinfo",
    //             name: 'newtailinfo',
    //             meta: {
    //               title: "详情",
    //               keepShow: false,
    //               role: [], //权限设置
    //               icon: 'el-icon-s-management' //菜单左侧的icon图标
    //             },
    //             component: '/business/buildInformManage/newtailinfo'
    //           },
    //         ]
    //       },
    //       {
    //         id: '43',
    //         name: 'addressBookManage',
    //         path: '/business/addressBookManage',
    //         meta: {
    //           title: '通讯录管理',
    //           keepShow: true,
    //           role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'AE', 'infoCenter'], //权限设置
    //           icon: 'el-icon-s-unfold' //菜单左侧的icon图标
    //         },
    //         component: '/business/addressBook/index',
    //         children: [
    //           {
    //             name: 'addressBookList',
    //             path: 'addressBookList',
    //             meta: {
    //               title: '通讯录',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter'], //权限设置
    //               icon: 'el-icon-phone-outline' //菜单左侧的icon图标
    //             },
    //             component: '/business/addressBook/addressBook',
    //           },
    //           {
    //             name: 'addressBookTmpl',
    //             path: 'addressBookTmpl',
    //             meta: {
    //               title: '模板管理',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter'], //权限设置
    //               icon: 'el-icon-document-copy' //菜单左侧的icon图标
    //             },
    //             component: '/business/addressBook/addressBookTmpl',
    //           },
    //           {

    //             name: 'networkCommunicate',
    //             path: 'networkCommunicate',
    //             meta: {
    //               title: '人脉通讯',
    //               keepShow: true,
    //               role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter'], //权限设置
    //               icon: 'el-icon-document-copy' //菜单左侧的icon图标
    //             },
    //             component: '/business/addressBook/networkCommunicate',
    //           },


    //         ],
    //         redirect: '/business/addressBookManage/addressBookList',
    //       },
    //       {
    //         id: '43',
    //         name: 'datumCenter',
    //         path: '/business/datumManage',
    //         meta: {
    //           title: '资料管理',
    //           keepShow: false,
    //           role: ['admin', 'manage', 'employee', 'ESalesman', 'BExecutor', 'BUM', 'PExecutor', 'PM', 'integrator', 'AE', 'infoCenter', 'visitor'],  //权限设置
    //           icon: 'el-icon-upload' //菜单左侧的icon图标
    //         },
    //         component: '/business/datumManage/datumcenter2',
    //       },
    //       // {
    //       //   name: 'iteminfoadmini',
    //       //   path: '/business/iteminfoadmini',
    //       //   meta: {
    //       //     title: '项目场景管理',
    //       //     keepShow: true,
    //       // role: ['admin', 'manage', 'employee','ESalesman','BExecutor','BUM','PExecutor','PM','integrator','AE','infoCenter'], //权限设置
    //       //     icon: 'el-icon-s-unfold' //菜单左侧的icon图标
    //       //   },
    //       //   component: '/business/Projectsceneadmin/index',
    //       //   children: [
    //       //     {
    //       //       name: 'projectSceneList',
    //       //       path: 'projectSceneList',
    //       //       meta: {
    //       //         title: '项目场景列表',
    //       //         keepShow: true,
    //       // role: ['admin', 'manage', 'employee','ESalesman','BExecutor','BUM','PExecutor','PM','integrator','AE','infoCenter'], //权限设置
    //       //         icon: 'el-icon-office-building' //菜单左侧的icon图标
    //       //       },
    //       //       component: '/business/Projectsceneadmin/projectSceneList',
    //       //     },
    //       //     {
    //       //       name: 'projectScene',
    //       //       path: 'projectScene',
    //       //       meta: {
    //       //         title: '项目场景添加',
    //       //         keepShow: true,
    //       // role: ['admin', 'manage', 'employee','ESalesman','BExecutor','BUM','PExecutor','PM','integrator','AE','infoCenter'], //权限设置
    //       //         icon: 'el-icon-office-building' //菜单左侧的icon图标
    //       //       },
    //       //       component: '/business/Projectsceneadmin/projectScene',
    //       //     }
    //       //   ]
    //       // },
    //       // {
    //       //   name: 'projectScene',
    //       //   path: '/business/Projectsceneadmin',
    //       //   meta: {
    //       //     title: '项目场景管理添加',
    //       //     icon: 'el-icon-office-building' //菜单左侧的icon图标
    //       //   },
    //       //   component: '/business/Projectsceneadmin/projectScene',
    //       // },
    //     ]
    //   }
    //   // =======================      商务部 end      =======================
    // ];

    // router;
  },

  // ============================================== 格式化 start ==============================================

  /**
  * 格式化省份
  * @param row 对象
  * @returns
  */
  fifterCitysName(row) {
    let city = "";
    row.province && row.province != "undefined" && row.province != "null" ? (city += row.province) : "";
    row.city && row.city != "undefined" && row.city != "null" ? (city += "-" + row.city) : "";
    row.area && row.area != "undefined" && row.area != "null" ? (city += "-" + row.area) : "";
    city ? city : (city = "无");
    return city;
  },

  // ============================================== 格式化 end ============================================== 


  /**
  * // 关闭弹窗清空值
  * @param formInfo 表单 
  * @param _this 指代
  * @returns
  */
  resetFormInfo(formInfo, _this) {
    return _this.$refs[formInfo] ? _this.$refs[formInfo].resetFields() : _this[formInfo] = {};
  },




  /**
* // 深度复制数组
* @param formInfo 表单 
* @param _this 指代
* @returns
*/
  deepCopyArr(obj) {
    const isObject = args => (typeof args === 'object' || typeof args === 'function') && typeof args !== null
    if (!isObject) throw new Error('Not Reference Types')
    let newObj = Array.isArray(obj) ? [...obj] : { ...obj }
    Reflect.ownKeys(newObj).map(key => {
      newObj[key] = isObject(obj[key]) ? this.deepCopyArr(obj[key]) : obj[key]
    })
    return newObj
  },



  /**
* // ele的所有图标
* @param 
* @param 
* @returns
*/
  getEleIcon() {
    return [
      "el-icon-platform-eleme",
      "el-icon-eleme",
      "el-icon-delete-solid",
      "el-icon-delete",
      "el-icon-s-tools",
      "el-icon-setting",
      "el-icon-user-solid",
      "el-icon-user",
      "el-icon-phone",
      "el-icon-phone-outline",
      "el-icon-more",
      "el-icon-more-outline",
      "el-icon-star-on",
      "el-icon-star-off",
      "el-icon-s-goods",
      "el-icon-goods",
      "el-icon-warning",
      "el-icon-warning-outline",
      "el-icon-question",
      "el-icon-info",
      "el-icon-remove",
      "el-icon-circle-plus",
      "el-icon-success",
      "el-icon-error",
      "el-icon-zoom-in",
      "el-icon-zoom-out",
      "el-icon-remove-outline",
      "el-icon-circle-plus-outline",
      "el-icon-circle-check",
      "el-icon-circle-close",
      "el-icon-s-help",
      "el-icon-help",
      "el-icon-minus",
      "el-icon-plus",
      "el-icon-check",
      "el-icon-close",
      "el-icon-picture",
      "el-icon-picture-outline",
      "el-icon-picture-outline-round",
      "el-icon-upload",
      "el-icon-upload2",
      "el-icon-download",
      "el-icon-camera-solid",
      "el-icon-camera",
      "el-icon-video-camera-solid",
      "el-icon-video-camera",
      "el-icon-message-solid",
      "el-icon-bell",
      "el-icon-s-cooperation",
      "el-icon-s-order",
      "el-icon-s-platform",
      "el-icon-s-fold",
      "el-icon-s-unfold",
      "el-icon-s-operation",
      "el-icon-s-promotion",
      "el-icon-s-home",
      "el-icon-s-release",
      "el-icon-s-ticket",
      "el-icon-s-management",
      "el-icon-s-open",
      "el-icon-s-shop",
      "el-icon-s-marketing",
      "el-icon-s-flag",
      "el-icon-s-comment",
      "el-icon-s-finance",
      "el-icon-s-claim",
      "el-icon-s-custom",
      "el-icon-s-opportunity",
      "el-icon-s-data",
      "el-icon-s-check",
      "el-icon-s-grid",
      "el-icon-menu",
      "el-icon-share",
      "el-icon-d-caret",
      "el-icon-caret-left",
      "el-icon-caret-right",
      "el-icon-caret-bottom",
      "el-icon-caret-top",
      "el-icon-bottom-left",
      "el-icon-bottom-right",
      "el-icon-back",
      "el-icon-right",
      "el-icon-bottom",
      "el-icon-top",
      "el-icon-top-left",
      "el-icon-top-right",
      "el-icon-arrow-left",
      "el-icon-arrow-right",
      "el-icon-arrow-down",
      "el-icon-arrow-up",
      "el-icon-d-arrow-left",
      "el-icon-d-arrow-right",
      "el-icon-video-pause",
      "el-icon-video-play",
      "el-icon-refresh",
      "el-icon-refresh-right",
      "el-icon-refresh-left",
      "el-icon-finished",
      "el-icon-sort",
      "el-icon-sort-up",
      "el-icon-sort-down",
      "el-icon-rank",
      "el-icon-loading",
      "el-icon-view",
      "el-icon-c-scale-to-original",
      "el-icon-date",
      "el-icon-edit",
      "el-icon-edit-outline",
      "el-icon-folder",
      "el-icon-folder-opened",
      "el-icon-folder-add",
      "el-icon-folder-remove",
      "el-icon-folder-delete",
      "el-icon-folder-checked",
      "el-icon-tickets",
      "el-icon-document-remove",
      "el-icon-document-delete",
      "el-icon-document-copy",
      "el-icon-document-checked",
      "el-icon-document",
      "el-icon-document-add",
      "el-icon-printer",
      "el-icon-paperclip",
      "el-icon-takeaway-box",
      "el-icon-search",
      "el-icon-monitor",
      "el-icon-attract",
      "el-icon-mobile",
      "el-icon-scissors",
      "el-icon-umbrella",
      "el-icon-headset",
      "el-icon-brush",
      "el-icon-mouse",
      "el-icon-coordinate",
      "el-icon-magic-stick",
      "el-icon-reading",
      "el-icon-data-line",
      "el-icon-data-board",
      "el-icon-pie-chart",
      "el-icon-data-analysis",
      "el-icon-collection-tag",
      "el-icon-film",
      "el-icon-suitcase",
      "el-icon-suitcase-1",
      "el-icon-receiving",
      "el-icon-collection",
      "el-icon-files",
      "el-icon-notebook-1",
      "el-icon-notebook-2",
      "el-icon-toilet-paper",
      "el-icon-office-building",
      "el-icon-school",
      "el-icon-table-lamp",
      "el-icon-house",
      "el-icon-no-smoking",
      "el-icon-smoking",
      "el-icon-shopping-cart-full",
      "el-icon-shopping-cart-1",
      "el-icon-shopping-cart-2",
      "el-icon-shopping-bag-1",
      "el-icon-shopping-bag-2",
      "el-icon-sold-out",
      "el-icon-sell",
      "el-icon-present",
      "el-icon-box",
      "el-icon-bank-card",
      "el-icon-money",
      "el-icon-coin",
      "el-icon-wallet",
      "el-icon-discount",
      "el-icon-price-tag",
      "el-icon-news",
      "el-icon-guide",
      "el-icon-male",
      "el-icon-female",
      "el-icon-thumb",
      "el-icon-cpu",
      "el-icon-link",
      "el-icon-connection",
      "el-icon-open",
      "el-icon-turn-off",
      "el-icon-set-up",
      "el-icon-chat-round",
      "el-icon-chat-line-round",
      "el-icon-chat-square",
      "el-icon-chat-dot-round",
      "el-icon-chat-dot-square",
      "el-icon-chat-line-square",
      "el-icon-message",
      "el-icon-postcard",
      "el-icon-position",
      "el-icon-turn-off-microphone",
      "el-icon-microphone",
      "el-icon-close-notification",
      "el-icon-bangzhu",
      "el-icon-time",
      "el-icon-odometer",
      "el-icon-crop",
      "el-icon-aim",
      "el-icon-switch-button",
      "el-icon-full-screen",
      "el-icon-copy-document",
      "el-icon-mic",
      "el-icon-stopwatch",
      "el-icon-medal-1",
      "el-icon-medal",
      "el-icon-trophy",
      "el-icon-trophy-1",
      "el-icon-first-aid-kit",
      "el-icon-discover",
      "el-icon-place",
      "el-icon-location",
      "el-icon-location-outline",
      "el-icon-location-information",
      "el-icon-add-location",
      "el-icon-delete-location",
      "el-icon-map-location",
      "el-icon-alarm-clock",
      "el-icon-timer",
      "el-icon-watch-1",
      "el-icon-watch",
      "el-icon-lock",
      "el-icon-unlock",
      "el-icon-key",
      "el-icon-service",
      "el-icon-mobile-phone",
      "el-icon-bicycle",
      "el-icon-truck",
      "el-icon-ship",
      "el-icon-basketball",
      "el-icon-football",
      "el-icon-soccer",
      "el-icon-baseball",
      "el-icon-wind-power",
      "el-icon-light-rain",
      "el-icon-lightning",
      "el-icon-heavy-rain",
      "el-icon-sunrise",
      "el-icon-sunrise-1",
      "el-icon-sunset",
      "el-icon-sunny",
      "el-icon-cloudy",
      "el-icon-partly-cloudy",
      "el-icon-cloudy-and-sunny",
      "el-icon-moon",
      "el-icon-moon-night",
      "el-icon-dish",
      "el-icon-dish-1",
      "el-icon-food",
      "el-icon-chicken",
      "el-icon-fork-spoon",
      "el-icon-knife-fork",
      "el-icon-burger",
      "el-icon-tableware",
      "el-icon-sugar",
      "el-icon-dessert",
      "el-icon-ice-cream",
      "el-icon-hot-water",
      "el-icon-water-cup",
      "el-icon-coffee-cup",
      "el-icon-cold-drink",
      "el-icon-goblet",
      "el-icon-goblet-full",
      "el-icon-goblet-square",
      "el-icon-goblet-square-full",
      "el-icon-refrigerator",
      "el-icon-grape",
      "el-icon-watermelon",
      "el-icon-cherry",
      "el-icon-apple",
      "el-icon-pear",
      "el-icon-orange",
      "el-icon-coffee",
      "el-icon-ice-tea",
      "el-icon-ice-drink",
      "el-icon-milk-tea",
      "el-icon-potato-strips",
      "el-icon-lollipop",
      "el-icon-ice-cream-square",
      "el-icon-ice-cream-round"
    ];
  }

}
