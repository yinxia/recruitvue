
import ElementUI from 'element-ui'
import './assets/reset.scss'
import './assets/theme/index.css'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import 'swiper/dist/css/swiper.css'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
Vue.config.productionTip = false
import banners from './components/navigation.vue'
Vue.prototype.$Baseimgurlrequst=`${process.env.VUE_APP_URL}api/attach/pub/img/`
Vue.component('top-banner',
 banners
)
Vue.prototype.$BaseimgurlrequstNotfind=require('./assets/indexImages/notimg.jpg')
Vue.use(ElementUI)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

// 广告链接
import adlinke from './uitls/Adlink'
// adlinke()
//
//



