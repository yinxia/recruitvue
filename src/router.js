import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
import global from '@/global';

const user = global.getCookie("assenToken");
const mian = () => import('./views/mian.vue')
const enterprise = () => import('./views/audit/enterprise.vue')
const personal = () => import('./views/audit/personal.vue')
const login = () => import('./views/login.vue')
const index = () => import('./views/index.vue')
const factory = () => import('./views/recruitmentInfo/factory.vue')
const design = () => import('./views/recruitmentInfo/design.vue')
const quickRecruit = () => import('./views/recruitmentInfo/quickRecruit.vue')
const moreRecruitment = () => import('./views/recruitmentInfo/moreRecruitment.vue')
const archivesMouths = () => import('./views/recruitmentInfo/archivesMouths.vue')
const electricityAnchor = () => import('./views/recruitmentInfo/electricityAnchor.vue')
const temporaryEmployment = () => import('./views/jobInformation/temporaryEmployment.vue')
const IndividualJob = () => import('./views/jobInformation/IndividualJob.vue')
const cooperationProcessing = () => import('./views/releases/cooperationProcessing.vue')
const transfer = () => import('./views/secondTransfer/transfer.vue')
const wantBuy = () => import('./views/secondTransfer/wantBuy.vue')
const embroideryWater = () => import('./views/embroidery/embroideryWater.vue')
const accessories = () => import('./views/clothAccessories/accessories.vue')
const rentHouse = () => import('./views/rentHouse/rentHouse.vue')
const postArticle = () => import('./views/postArticle/postArticle.vue')
const incomeDetails = () => import('./views/incomeDetails/incomeDetails.vue')

// const Recruits = () => import('./views/Recruits.vue')

export default new Router({
  routes: [
   {
    path: '/',
    name: 'home',
    component: mian,
    children:[
      {

        path: 'enterprise',
        name: 'enterprise',
        component: enterprise,
        meta: {
          keepAlive: true
        }
      },
      
      {
        path: 'personal',
        name: 'personal',
        component: personal,
        meta: {
          keepAlive: true
        }
      },
      {
        path: 'login',
        name: 'login',
        component: login,
        meta: {
            keepAlive: false
        }      
      },
      {
        path: 'index',
        name: 'index',
        component: index,
        meta: {
            keepAlive: true
        }      
      },

      {
        path: 'design',
        name: 'design',
        component: design,
        meta: {
            keepAlive: true
        }      
      },
      {
        path: 'factory',
        name: 'factory',
        component: factory,
        meta: {
            keepAlive: true
        }      
      },
      {
        path: 'quickRecruit',
        name: 'quickRecruit',
        component: quickRecruit,
        meta: {
            keepAlive: true
        }      
      },
      {
        path: 'moreRecruitment',
        name: 'moreRecruitment',
        component: moreRecruitment,
        meta: {
            keepAlive: true
        }      
      },
      {
        path: 'archivesMouths',
        name: 'archivesMouths',
        component: archivesMouths,
        meta: {
            keepAlive: true
        }      
      },
      {
        path: 'electricityAnchor',
        name: 'electricityAnchor',
        component: electricityAnchor,
        meta: {
            keepAlive: true
        }      
      },
      
      {
        path: 'temporaryEmployment',
        name: 'temporaryEmployment',
        component: temporaryEmployment,
        meta: {
            keepAlive: true
        }      
      },
      
      {
        path: 'IndividualJob',
        name: 'IndividualJob',
        component: IndividualJob,
        meta: {
            keepAlive: true
        }      
      },

      {
        path: 'cooperationProcessing',
        name: 'cooperationProcessing',
        component: cooperationProcessing,
        meta: {
            keepAlive: true
        }      
      },

      {
        path: 'transfer',
        name: 'transfer',
        component: transfer,
        meta: {
            keepAlive: true
        }      
      },
      
      {
        path: 'wantBuy',
        name: 'wantBuy',
        component: wantBuy,
        meta: {
            keepAlive: true
        }      
      },

      {
        path: 'embroideryWater',
        name: 'embroideryWater',
        component: embroideryWater,
        meta: {
            keepAlive: true
        }      
      },

      {
        path: 'accessories',
        name: 'accessories',
        component: accessories,
        meta: {
            keepAlive: true
        }      
      },

      {
        path: 'rentHouse',
        name: 'rentHouse',
        component: rentHouse,
        meta: {
            keepAlive: true
        }      
      },

      {
        path: 'postArticle',
        name: 'postArticle',
        component: postArticle,
        meta: {
            keepAlive: true
        }      
      },

      {
        path: 'incomeDetails',
        name: 'incomeDetails',
        component: incomeDetails,
        meta: {
            keepAlive: true
        }      
      },

    ]
  }
 ]
})

