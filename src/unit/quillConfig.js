
// 富文本编辑器头部功能配置
 export const modules=[
  ['bold', 'italic', 'underline', 'strike'], 
  ['blockquote', 'code-block'],
  [{'indent': '-1'}, {'indent': '+1'}],
  [{'size': ['small', false, 'large', 'huge']}],
  [{'header': [1, 2, 3, 4, 5, 6, false]}],
  [{'color': []}, {'background': []}],
  [{'align': []}],
  ['image',]
 ]
          
 
