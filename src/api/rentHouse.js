import axios from './serve'  


export const pagerList = (data) => {
    return axios.get('/api/rentHouse/pagerList', { params: data })
};


export const updateResults = (data) => {
    return axios.post('/api/rentHouse/updateResults', data)
};


export const batchResults = (data) => {
    return axios.post('/api/rentHouse/batchResults', data)
};

export const findCountNum3 = () => {
    return axios.get('/api/rentHouse/findCountNum')
};

