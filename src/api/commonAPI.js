import axios3 from '@/api/serve.js';
// import axios2 from '@/api/server.js';
// import axios from '../server.js';
import CryptoJS from 'crypto-js';
import global from '@/global';

let formatDate = function (date, type) {
  let returnData;
  var y = date.getFullYear();
  var m = date.getMonth() + 1;
  m = m < 10 ? '0' + m : m;
  var d = date.getDate();
  d = d < 10 ? ('0' + d) : d;

  var h = date.getHours();
  h = h < 10 ? ('0' + h) : h;
  var minute = date.getMinutes();
  minute = minute < 10 ? ('0' + minute) : minute;
  var second = date.getSeconds();
  second = second < 10 ? ('0' + second) : second;
  var week = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"][date.getDay()];
  switch (type) {
    case '-':
      returnData = (y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second)
      break;
    case '年月日':
      returnData = (y + '年' + m + '月' + d + '日 ' + h + ':' + minute)
      // + ':' + second
      break;
    // case '':
    //   break;
    default:
      returnData = (y + m + d);
      break;
  }
  return returnData;
  // return y + '-' + m + '-' + d;
};

/**
 *   返回时间
 * @export
 * @param {*} data ——数据
 * @param {*} type —— 数据格式
 */
export const resetDate = (data, type) => {
  return formatDate(data, type)
};

/**
 *   文件下载
 * @export
 * @param {*} data ——数据
 */
export const exportFileInfo = (data) => {
  let access_token = global.getCookie('assenToken');
  return `${process.env.VUE_APP_URL}file/api/uploadFile/download?id=${data}&access_token=${access_token}`
};

/**
 *   文件下载
 * @export
 * @param {*} data ——数据
 */
export const exportFileTemplate = (data) => {
  let access_token = global.getCookie('assenToken');
  return `${process.env.VUE_APP_URL}project/api/integratorPolicyInfo/templateExport?integratorPolicyId=${data}&access_token=${access_token}`
};
//   文件下载导出
export const exportFileTemplateexprot = (data) => {
  let access_token = global.getCookie('assenToken');
  return `${process.env.VUE_APP_URL}project/api/integratorPolicyInfo/exportExcel?${data}&access_token=${access_token}`
};


/**
 * 带参数导出【！！！只能是ms-excel文件】
 * @export
 * @param {*} data ——数据
 */
export const doExport = (gateway, url, data, fileName) => {
  axios3.defaults.responseType = 'blob';
  axios3.get(gateway + url, data).then(res => {
    if (res.data) {
      let date = new Date();
      let name = (fileName || "导出文件") + formatDate(date);
      let blob = new Blob([res.data], {
        type: "application/vnd.ms-excel;charset=utf-8"//excel 93-2003后缀为 .xls
      }); //application/vnd.openxmlformats-officedocument.spreadsheetml.sheet //excel2007格式后缀是 .xlsx
      if ("download" in document.createElement("a")) {// 非IE下载
        let link = document.createElement("a");
        link.setAttribute('download', name)
        link.setAttribute('href', window.URL.createObjectURL(blob))
        link.click()
        link.remove();
        window.URL.revokeObjectURL(blob);
      } else {
        // navigator.msSaveBlob(blob, name);
        window.navigator.msSaveOrOpenBlob(blob, fileName)
      }
    }


  });
  return
};

// /**
//  * des机密
//  * @export
//  * @param {*} data ——数据
//  */
// export function encryptByDES(str) {
//   // const key1 = 'eyJhbGciOiJIUzI1NiJ9';
//   const key1 = 'sixduaeslogin123';
//   // TripleDES
//   const keyHex = CryptoJS.enc.Utf8.parse(key1);
//   const encrypted = CryptoJS.TripleDES.encrypt(window.btoa(str), keyHex, {
//     iv: keyHex,
//     mode: CryptoJS.mode.ECB,
//     padding: CryptoJS.pad.Pkcs7,
//   });
//   return encrypted.toString();
// }
/**
 * AES机密
 * @export
 * @param {*} data ——数据
 */
export function encryptByAES(str) {
  var key = 'sixduaeslogin123';
  var keyHex = CryptoJS.enc.Utf8.parse(key);
  var srcs = CryptoJS.enc.Utf8.parse(str);
  const encrypted = CryptoJS.AES.encrypt(srcs, keyHex, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7,
  });
  return encrypted.toString();
}

// export function decryptByAES(str) {
//   var keyHex = CryptoJS.enc.Utf8.parse('sixduaeslogin123');
//   var decrypt = CryptoJS.AES.decrypt(str, keyHex, {mode:CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});  
//   console.log(CryptoJS.enc.Utf8.stringify(decrypt).toString())
//   return CryptoJS.enc.Utf8.stringify(decrypt).toString();  
// }


/**
 * 拼接对象为请求字符串
 * @param {Object} obj - 待拼接的对象
 * @returns {string} - 拼接成的请求字符串
 */
export function encodeSearchParams(obj) {
  const params = []

  Object.keys(obj).forEach((key) => {
    let value = obj[key]
    // 如果值为undefined我们将其置空
    if (typeof value === 'undefined') {
      value = ''
    }
    // 对于需要编码的文本（比如说中文）我们要进行编码
    params.push([key, encodeURIComponent(value)].join('='))
  })

  return params.join('&')
}
// /**
//  * 高德云图
//  * @export
//  * @param {*} datakey 客户唯一标识
//  * @param {*} tableid 数据表唯一标识
//  * @param {*} file 数据表唯一标识 1、csv文件的二进制流，使用UTF8、GBK编码，字段总数不超过40个。
//  *                               2、数据量不超过10000条且文件大小不超过10M。
//  *                               3、字段命名规则：以英文字母开头，仅由英文字母、数字、下划线组成，总长度不超过20位。
//  * @param {*} _name 文件中代表”名称”的字段
//  * @param {*} loctype 文件中代表”名称”的字段 1：经纬度；格式示例：104.394729,31.125698 2：地址
//  * @param {*} _address 文件中代表”地址”的字段 当loctype=2时，必填
//  * @param {*} longitude 文件中代表”经度”（格式示例：104.394729）的字段 当loctype=1时，必填
//  * @param {*} latitude 文件中代表”纬度”（格式示例：31.125698）的字段 当loctype=1时，必填
//  * @param {*} coordtype 坐标类型 1: gps 2: autonavi 3: baidu 您可输入coordtype=1,或者autonavi 仅当定位方式loctype=1时，该字段有效
//  * @param {*} sig 数字签名
//  */
// export const mapBatchCreate = (data) => {
//   return axios2.post('https://yuntuapi.amap.com/datamanage/data/batchcreate', data)

// };

