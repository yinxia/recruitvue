import axios from './serve'  


export const pagerList = (data) => {
    return axios.get('/api/jobInformation/pagerList', { params: data })
};


export const details = (data) => {
    return axios.get('/api/jobInformation/details', { params: data })
};

export const updateResults = (data) => {
    return axios.post('/api/jobInformation/updateResults', data)
};

export const batchResults = (data) => {
    return axios.post('/api/jobInformation/batchResults', data)
};


export const findCountNum1 = () => {
    return axios.get('/api/jobInformation/findCountNum')
};

export const findCountNumAndReleaseType1 = (data) => {
    return axios.get('/api/jobInformation/findCountNumAndReleaseType', { params: data })
};