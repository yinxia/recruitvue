import axios from './serve'  


export const pagerList = (data) => {
    return axios.get('/api/order/pagerList', { params: data })
};

export const sumMoney = (data) => {
    return axios.get('/api/order/sumMoney', { params: data })
};


