import axios from './serve'  


export const pagerList = (data) => {
    return axios.get('/api/postArticle/pagerList', { params: data })
};


export const updateResults = (data) => {
    return axios.post('/api/postArticle/updateResults', data)
};


export const batchResults = (data) => {
    return axios.post('/api/postArticle/batchResults', data)
};

export const findCountNum4 = () => {
    return axios.get('/api/postArticle/findCountNum')
};
