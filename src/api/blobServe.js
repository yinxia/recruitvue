import axios from 'axios';
import { Loading, Message, MessageBox } from 'element-ui';
// import qs from 'qs';
import global from '@/global'

// let token = getCookie('access_token');
const $http = axios.create()
$http.defaults.withCredentials = true;
$http.baseURL = process.env.VUE_APP_URL;
let resetURL = '';
process.env.NODE_ENV == 'text' ?
  resetURL = 'http://localhost:8099/' :
  (process.env.NODE_ENV == 'development' ?
    resetURL = 'http://192.168.0.185:8097/' : resetURL = 'http://sixdu.bj6das.com/');
var loadinginstace;


// 请求配置 request interceptor
$http.interceptors.request.use((config) => {
  let token = global.getCookie('assenToken');
  if (token && token !== 'undefined') {
    config.headers = {
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'multipart/form-data;boundary = ' + new Date().getTime()
    }

    config.transformRequest = [function (data) {
      return data
    }]
    // config.data = qs.stringify(config.data, { arrayFormat: 'repeat' });

  } else {
    Message({
      message: '未授权，登录状态已经过期,请重新登录',
      type: 'error',
      duration: 1500,
      onClose() {
        return window.location.href = resetURL + '#/login';
      }
    });
  }
  // // 记录post 和 put 操作
  //   if (config.method !== 'get' && config.method !== 'delete') {
  //     // ------------------------------------------------------------------------------------
  //     config.cancelToken = new CancelToken((c) => {
  //       // 这里的ajax标识使用用请求地址&请求方式拼接的字符串
  //       checkPending({ ajaxKey: `${config.url}&${config.method}`, executor: c });
  //     });
  //     // -----------------------------------------------------------------------------------------
  //   }
  loadinginstace = Loading.service({
    lock: true,
    text: '努力加载中。。。',
    spinner: 'el-icon-loading',
    background: 'rgba(0, 0, 0, 0.3)',
    customClass: "osloading",
    fullscreen: true
  })
  return config
},
  (error) => {
    loadinginstace.close()
    return Promise.reject(error)
  });
// 响应拦截器 response interceptor
$http.interceptors.response.use((data) => {// 响应成功关闭loading
  loadinginstace.close()
  return data
}, (error) => {
  loadinginstace.close();
  if (error && error.code) {
    switch (error.code) {
      case 2:
        Message({
          message: '未授权，登录状态已经过期,请重新登录',
          type: 'error',
          duration: 3000,
          onClose() {
            return window.location.href = resetURL + '#/login';
          }
        });
    }
  } else if (error && error.response) {
    switch (error.response.status) {
      case 400:
        error.message = '错误请求';
        break;
      case 401:
        Message({
          message: '未授权，请重新登录',
          type: 'error',
          duration: 3000,
          onClose() {
            return window.location.href = resetURL + '#/login';
          }
        });
        return;
      case 403:
        error.message = '拒绝访问';
        break;
      case 404:
        error.message = '请求错误,未找到该资源';
        break;
      case 405:
        error.message = '请求方法未允许';
        break;
      case 408:
        error.message = '请求超时';
        break;
      case 500:
        error.message = '服务器端出错';
        break;
      case 501:
        error.message = '网络未实现';
        break;
      case 502:
        error.message = '网络错误';
        break;
      case 503:
        error.message = '服务不可用';
        break;
      case 504:
        error.message = '网络超时';
        break;
      case 505:
        error.message = '抱歉！您当前的浏览器版本过低，可能存在安全风险，请升级浏览器至IE10及以上版本。';
        break;
      default:
        error.message = `连接错误${error.response.status}`
    }
    // Loading.service(options).close();
    // Message({
    //   message: error.message,
    //   type: 'error',
    //   duration: 3000
    // });
    // return Promise.reject(error.response.data)
  } else {
    error.message = '服务错误，请联系管理员，稍后再试！'
  }
  Message({
    message: error.message,
    type: 'error',
    duration: 3000
  });


  // return Promise.reject(error)
  return Promise.resolve(error)
}
);
export default $http;