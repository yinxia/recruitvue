import axios from './serve'  


export const getPagerAllList = (data) => {
    return axios.get('/api/authentication/getPagerAllList', { params: data })
};


export const details = (data) => {
    return axios.get('/api/authentication/details', { params: data })
};

export const updateResults = (data) => {
    return axios.post('/api/authentication/updateResults', data)
};

export const batchResults = (data) => {
    return axios.post('/api/authentication/batchResults', data)
};

export const findCountResults = () => {
    return axios.get('/api/authentication/findCountResults')
};

export const findCountResultsAndType = (data) => {
    return axios.get('/api/authentication/findCountResultsAndType', { params: data })
};
