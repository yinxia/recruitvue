import axios from './serve'  

export const getPagerAllList = (data) => {
    return axios.get('/api/factoryRecruitment/pagerList', { params: data })
};

export const details = (data) => {
    return axios.get('/api/factoryRecruitment/details', { params: data })
};

export const updateResults = (data) => {
    return axios.post('/api/factoryRecruitment/updateResults', data)
};

export const batchResults = (data) => {
    return axios.post('/api/factoryRecruitment/batchResults', data)
};

export const findCountResults1 = () => {
    return axios.get('/api/factoryRecruitment/findCountResults')
};

export const findCountRecruitType = (data) => {
    return axios.get('/api/factoryRecruitment/findCountRecruitType', { params: data })
};
