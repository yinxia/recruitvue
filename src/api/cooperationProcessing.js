import axios from './serve'  


export const pagerList = (data) => {
    return axios.get('/api/releases/pagerList', { params: data })
};


export const updateResults = (data) => {
    return axios.post('/api/releases/updateResults', data)
};


export const batchResults = (data) => {
    return axios.post('/api/releases/batchResults', data)
};

export const findCountNum2 = (data) => {
    return axios.get('/api/releases/findCountNum',{ params: data })
};

export const findCountNumAndReleaseType2 = (data) => {
    return axios.get('/api/releases/findCountNumAndType', { params: data })
};