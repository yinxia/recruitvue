import axios from 'axios';
import { Loading, Message, MessageBox } from 'element-ui';
import qs from 'qs';
import global from '../global'
const $http = axios.create();
$http.defaults.timeout = 10000;
$http.defaults.withCredentials = true;// axios 默认不发送cookie，需要全局设置true发送cookie
$http.defaults.baseURL = process.env.VUE_APP_URL;
let resetURL = '';
process.env.NODE_ENV == 'text' ?
 resetURL = 'http://192.168.0.185:8095/' :
 (process.env.NODE_ENV == 'production' ?
    resetURL = 'http://192.168.0.185:8095/' : resetURL = 'http://127.0.0.1:8095/');
// const resetURL ='http://localhost:8099/';
// var loadinginstace:any;
let loadinginstace

// 请求拦截器
// let options = {
//     text: '载入中...',
//     spinner: '',
//     background: 'rgba(255,255,255,0.7)'
// };

// /**
//  *请求执行时查看是否已有正在请求中的相同请求，有的话取消当次请求， 没有则记录当次请求
//  *
//  * @param {*} ever 请求的config数据
//  * @returns
//  */
// const checkPending = (ever) => {
//   const pending = pendings.find(item => item.ajaxKey === ever.ajaxKey);
//   // 如果当前请求被记录则执行当次请求取消操作
//   if (pending) {
//     ever.executor('重复请求'); // 执行取消操作
//     console.info('重复请求', ever.ajaxKey);
//   } else {
//     pendings.push(ever); // 反之把这条记录添加进数组
//   }
// };
// /**
//  *请求完成后从记录中移除
//  *
//  * @param {*} ever 请求的config数据
//  * @returns
//  */
// const achievePending = (ever) => {
//   const pendingIndex = pendings.findIndex(item => item.ajaxKey === `${ever.url}&${ever.method}`);
//   if (pendingIndex === -1) return; // 如果当前请求未被记录则不需要执行删除操作
//   pendings.splice(pendingIndex, 1); // 把这条记录从数组中移除
// };
// /**
//  *请求完成时计数器减1，为0时关闭loading框
//  *
//  */
// const minusLoadingcount = () => {
//   loadingcount -= 1; // 请求返回-1
//   clearTimeout(Lib.loadingTime);
//   if (loadingcount <= 0) {
//     // 当所有请求完成后关闭
//     Lib.loadingInstance.$el.style.opacity = 0;
//     Lib.loadingInstance.close();
//   }
// };

// 请求配置 request interceptor
$http.interceptors.request.use((config) => {
  let token = global.getCookie('assenToken');
  if (token && token !== 'undefined') {
    config.headers['Authorization'] = 'Bearer ' + token;
    config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    config.data = qs.stringify(config.data, { arrayFormat: 'repeat' });
  } else {
    Message({
      message: '未授权，登录状态已经过期,请重新登录',
      type: 'error',
      duration: 1500,
      onClose() {
        // MessageBox.alert('<iframe id="idFrame" name="idFrame" src="' + global.URL + '/#/login.html" width="500" height="500" frameborder="0" scrolling="auto"></iframe>', '登录', {
        //   showConfirmButton: false,
        //   dangerouslyUseHTMLString: true
        // }); 
        return window.location.href = resetURL + '#/login';
      }
    });
  }
  // // 记录post 和 put 操作
  //   if (config.method !== 'get' && config.method !== 'delete') {
  //     // ------------------------------------------------------------------------------------
  //     config.cancelToken = new CancelToken((c) => {
  //       // 这里的ajax标识使用用请求地址&请求方式拼接的字符串
  //       checkPending({ ajaxKey: `${config.url}&${config.method}`, executor: c });
  //     });
  //     // -----------------------------------------------------------------------------------------
  //   }

  loadinginstace = Loading.service({
    lock: true,
    text: '努力加载中。。。',
    spinner: 'el-icon-loading',
    background: 'rgba(0, 0, 0, 0.3)',
    customClass: "osloading",
    fullscreen: true
  })
  return config
},
  (error) => {
    // console.log(error)
    loadinginstace.close()
    return Promise.reject(error)
  });
// 响应拦截器 response interceptor
$http.interceptors.response.use((data) => {// 响应成功关闭loading
  // Loading.service(options).close();
  // console.log(data)
  // console.log(data)
  loadinginstace.close()
  return data
}, (error) => {
  // if (error && error.code) {
  //  
  //   // switch (error.code) {
  //   //   case 2:
  //   //     Message({
  //   //       message: '未授权，登录状态已经过期,请重新登录',
  //   //       type: 'error',
  //   //       duration: 3000,
  //   //       onClose() {
  //   //         return window.location.href = resetURL + '#/login';
  //   //       }
  //   //     });
  //   //     break;

  //   // }
  // } else 
  // console.log(error)
  if (error && error.response) {
    switch (error.response.status) {
      case 400:
        error.message = '错误请求';
        break;
      case 401:
        Message({
          message: '未授权，请重新登录',
          type: 'error',
          duration: 3000,
          onClose() {
            return window.location.href = resetURL + '#/login';
          }
        });
        return;
      case 403:
        error.message = '拒绝访问';
        break;
      case 404:
        error.message = '请求错误,未找到该资源';
        break;
      case 405:
        error.message = '请求方法未允许';
        break;
      case 408:
        error.message = '请求超时';
        break;
      case 500:
        error.message = '服务器端出错';
        break;
      case 501:
        error.message = '网络未实现';
        break;
      case 502:
        error.message = '网络错误';
        break;
      case 503:
        error.message = '服务不可用';
        break;
      case 504:
        error.message = '网络超时';
        break;
      case 505:
        error.message = '抱歉！您当前的浏览器版本过低，可能存在安全风险，请升级浏览器至IE10及以上版本。';
        break;
      default:
        error.message = `连接错误${error.response.status}`
    }
    // Loading.service(options).close();
    // Message({
    //   message: error.message,
    //   type: 'error',
    //   duration: 3000
    // });
    // return Promise.reject(error.response.data)
  } else {
    error.message = '服务错误，请联系管理员，稍后再试！'
  }
  Message({
    message: error.message,
    type: 'error',
    duration: 3000
  });
  // return Promise.reject(error)  
  var config = error.config;
  // If config does not exist or the retry option is not set, reject
  // 如果config不存在或者是空的就不继续或者拒绝。
  if (!config || !config.retry) return Promise.reject(error);
  // Set the variable for keeping track of the retry count
  // 设置用于跟踪重请求计数的变量
  config.__retryCount = config.__retryCount || 0;
  // Check if we've maxed out the total number of retries
  // 检查我们是否已经把重请求的总数用完
  if (config.__retryCount >= config.retry) {
    // Reject with the error
    // 带错误拒绝
    return Promise.reject(error);
  }
  // Increase the retry count
  // 增加重请求计数
  config.__retryCount += 1;
  // Create new promise to handle exponential backoff
  // 创造新的承诺来处理指数后退
  var backoff = new Promise(function (resolve) {
    setTimeout(function () {
      resolve();
    }, config.retryDelay || 1);
  });
  // Return the promise in which recalls axios to retry the request
  // 返回要求axios重试请求的承诺
  return backoff.then(function () {
    return $http(config);
  });
  // return ;
});
export default $http;

