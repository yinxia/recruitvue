FROM  node:latest

ADD . recruit-vue/

WORKDIR /recruit-vue

RUN yarn install

ENTRYPOINT ["yarn","run","serve"]